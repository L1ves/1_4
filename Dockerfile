FROM debian:9
RUN apt-get update -y \
&& apt-get upgrade -y \
&& apt-get -y install gcc \
&& apt-get -y install libpcre3 libpcre3-dev \
&& apt-get -y install zlib1g-dev \
&& apt-get -y install wget \
&& apt-get -y install build-essential libssl-dev \
&& apt-get -y install git \
&& apt-get clean -y 


RUN wget https://nginx.org/download/nginx-1.23.3.tar.gz \
&& tar -xzf nginx-1.23.3.tar.gz 
RUN git clone https://github.com/openresty/lua-nginx-module.git
RUN cd nginx-1.23.3 \
&& chmod +x ./configure \
&& ./configure --add-module=../lua-nginx-module \
&& make && make install
COPY nginx.conf usr/local/nginx/conf/nginx.conf
VOLUME ["/usr/local/nginx/conf"]
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]

